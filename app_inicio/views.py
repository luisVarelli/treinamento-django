from django.shortcuts import render

from app_inicio.models import *
from app_inicio.forms import ArtigoForm

def post_list(request):
    #import ipdb; ipdb.set_trace() #para debug no terminal
    if request.method == 'POST':
        form = ArtigoForm(request.POST)
        if form.is_valid():
            filtro = form.cleaned_data['title']
            if filtro:
                posts = Artigos.objects.filter(title=filtro)
                return render(request, 'app_inicio/post_list.html', {'posts': posts, 'form': form})
    form = ArtigoForm()
    posts = Artigos.objects.all()
    return render(request, 'app_inicio/post_list.html', {'posts': posts, 'form': form})
