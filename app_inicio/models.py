# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils import timezone

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class News_Type(models.Model):
    description = models.CharField(max_length=200)


class News(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    news_type = models.ForeignKey('News_Type')


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default=timezone.now)


STATUS_CHOICES = (
    ('d', 'Draft'),
    ('p', 'Published'),
    ('w', 'Withdrawn'),
)

class Artigos(models.Model):
    title = models.CharField(max_length=100, verbose_name = "Titulo")
    body = models.TextField(verbose_name=u"Descrição")
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)

    def __str__(self):
        return self.title
    class Meta():
        db_table = "Artigos" #seta o nome literal da tabela
        #ordering = ['campo']
        verbose_name_plural = u"Administração Artigos"
     #define qual campo irá aparecer como principal na tela
    def __unicode__(self):
        return self.title
