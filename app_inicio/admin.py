from django.contrib import admin
from app_inicio.models import Artigos
from app_inicio.forms import ArtigoForm

from app_inicio.models import Post,News_Type,News,Question

admin.site.site_header = "Administrador do site"

def make_published(modeladmin, request, queryset):
    queryset.update(status='p')
make_published.short_description = "Mark selected stories as published"

class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'status', 'body']
    ordering = ['title']
    actions = [make_published]    

admin.site.register(Artigos, ArticleAdmin)
admin.site.register(Post)
admin.site.register(News)
admin.site.register(News_Type)
admin.site.register(Question)
